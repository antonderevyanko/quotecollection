package com.derevyanko.quotescollection.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.derevyanko.quotescollection.R;
import com.derevyanko.quotescollection.adapter.QuotesProvider;
import com.derevyanko.quotescollection.adapter.QuotesRecyclerAdapter;
import com.derevyanko.quotescollection.entity.Quote;
import com.derevyanko.quotescollection.eventbus.EventCollection;

import java.util.List;

import com.derevyanko.quotescollection.util.FirstItemOffsetDecoration;
import de.greenrobot.event.EventBus;

public abstract class QuotesListFragment extends Fragment {

    protected static final String FRAGMENT_ARG = "quotes list fragment list";
    private List<Quote> quotesList;
    protected QuotesRecyclerAdapter adapter;
    protected LinearLayoutManager layoutManager;

    @InjectView(R.id.tvNoItems)
    TextView noItems;
    @InjectView(R.id.lvList)
    RecyclerView listView;
    @InjectView(R.id.upperImage)
    ImageView upperImage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_quotes_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        layoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(layoutManager);
        listView.addItemDecoration(new FirstItemOffsetDecoration(getActivity()));
        adapter = new QuotesRecyclerAdapter(quotesList);
        listView.setAdapter(adapter);
        Animation startAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.bottom_up);
        listView.startAnimation(startAnimation);
        listView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.reset(this);
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().registerSticky(this);
        EventBus.getDefault().post(new EventCollection.TitleEvent(getString(R.string.title_common)));
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        String quotesSource = getArguments().getString(FRAGMENT_ARG);
        quotesList = QuotesProvider.getQuotesFromString(quotesSource);
    }

    @SuppressWarnings("unused")
    public void onEvent(EventCollection.NotifyAdapterEvent notifyAdapterEvent) {
        EventBus.getDefault().removeStickyEvent(notifyAdapterEvent);
        adapter.notifyDataSetChanged();
    }
}