package com.derevyanko.quotescollection.fragment;

import android.app.Fragment;
import android.os.Bundle;

import android.view.View;
import com.derevyanko.quotescollection.R;
import com.derevyanko.quotescollection.adapter.QuotesProvider;
import com.derevyanko.quotescollection.entity.Quote;
import com.derevyanko.quotescollection.eventbus.EventCollection;

import java.util.List;

import de.greenrobot.event.EventBus;

public final class FavoriteQuotesFragment extends QuotesListFragment {

    public static Fragment newInstance(List<Quote> quotes) {
        Fragment fragment = new FavoriteQuotesFragment();
        Bundle args = new Bundle();
        args.putString(FRAGMENT_ARG, QuotesProvider.getQuotesAsString(quotes));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        checkEmptyState();
    }

    private void checkEmptyState() {
        if (adapter.getItemCount() == 0) {
            noItems.setVisibility(View.VISIBLE);
        } else {
            noItems.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new EventCollection.TitleEvent(getString(R.string.title_favorite)));
    }

    @SuppressWarnings("unused")
    public void onEvent(EventCollection.FavoriteClick favoriteEvent) {
        removeIfNeeded(favoriteEvent.isChecked(), favoriteEvent.getItemPosition());
    }

    @SuppressWarnings("unused")
    public void onEvent(EventCollection.NotifyAdapterEvent notifyAdapterEvent) {
        EventBus.getDefault().removeStickyEvent(notifyAdapterEvent);
        removeIfNeeded(notifyAdapterEvent.isChecked(), notifyAdapterEvent.getPosition());
    }

    private void removeIfNeeded(boolean checked, int itemPosition) {
        if (!checked) {
            adapter.removeItem(itemPosition);
            checkEmptyState();
        }
    }
}