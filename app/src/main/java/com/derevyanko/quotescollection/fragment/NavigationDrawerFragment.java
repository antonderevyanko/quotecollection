package com.derevyanko.quotescollection.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.derevyanko.quotescollection.R;
import com.derevyanko.quotescollection.adapter.NavigationAdapter;
import com.derevyanko.quotescollection.eventbus.EventCollection;

import com.derevyanko.quotescollection.view.NavListHeader;
import de.greenrobot.event.EventBus;

import java.util.*;

public class NavigationDrawerFragment extends Fragment {

    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    private ListView drawerListView;
    private int currentSelectedPosition = 0;

    public NavigationDrawerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            currentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
        } else {
            currentSelectedPosition = 0;
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(EventCollection.DrawerEvent.values()[currentSelectedPosition]);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        drawerListView = (ListView) inflater.inflate(
                R.layout.fragment_navigation_drawer, container, false);
        configureDrawerList();
        return drawerListView;
    }

    private void configureDrawerList() {
        String[] menuItems = getResources().getStringArray(R.array.menu_items);
        ArrayList<String> titles = new ArrayList<>(Arrays.asList(menuItems));
        drawerListView.addHeaderView(getHeaderView());
        drawerListView.setAdapter(new NavigationAdapter(getActivity(), R.layout.item_navigation, titles));
        drawerListView.setOnItemClickListener(menuItemClick);
        selectItem(currentSelectedPosition);
    }

    private View getHeaderView() {
        return new NavListHeader(getActivity());
    }

    private void selectItem(int position) {
        currentSelectedPosition = position;
        if (drawerListView != null) {
            drawerListView.setItemChecked(position + 1, true);
        }
        EventBus.getDefault().post(EventCollection.DrawerEvent.values()[position]);
    }

    private AdapterView.OnItemClickListener menuItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position - 1);
        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, currentSelectedPosition);
    }
}
