package com.derevyanko.quotescollection.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.derevyanko.quotescollection.R;
import com.derevyanko.quotescollection.eventbus.EventCollection;
import com.derevyanko.quotescollection.util.QuotePreferences;
import de.greenrobot.event.EventBus;

public final class SettingsFragment extends Fragment {

    private QuotePreferences quotePreferences;

    @InjectView(R.id.lvVersionList)
    ListView versionList;
    @InjectView(R.id.swDayTip)
    Switch tipOfTheDay;

    public static Fragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);
    }

    @Override
    public void onDestroyView() {
        ButterKnife.reset(this);
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tipOfTheDay.setOnCheckedChangeListener(tipDayListener);
        String[] versionHistory = getResources().getStringArray(R.array.history);
        versionList.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, versionHistory));
        quotePreferences = new QuotePreferences(getActivity());
        tipOfTheDay.setChecked(quotePreferences.isNeedToShowNotification());
    }

    private CompoundButton.OnCheckedChangeListener tipDayListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            quotePreferences.setShowNotification(isChecked);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new EventCollection.TitleEvent(getString(R.string.title_settings)));
    }
}