package com.derevyanko.quotescollection.fragment;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.derevyanko.quotescollection.R;
import com.derevyanko.quotescollection.adapter.QuotesProvider;
import com.derevyanko.quotescollection.entity.Quote;
import com.derevyanko.quotescollection.eventbus.EventCollection;
import de.greenrobot.event.EventBus;

import java.util.List;

public class RandomQuotesFragment extends QuotesListFragment {

    private static final float IMAGE_SCALE = 0.7f;
    private Animation rotation;
    private Animation appearNewAnimation;
    private Animation hideOldAnimation;
    private Bitmap oldBitmap;
    private Canvas canvas;

    public static Fragment newInstance(List<Quote> quotes) {
        Fragment fragment = new RandomQuotesFragment();
        Bundle args = new Bundle();
        args.putString(FRAGMENT_ARG, QuotesProvider.getQuotesAsString(quotes));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        // config animations
        rotation = AnimationUtils.loadAnimation(getActivity(), R.anim.rotation);
        rotation.setRepeatCount(0);
        appearNewAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.bottom_up);
        hideOldAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new EventCollection.TitleEvent(getString(R.string.title_common)));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_refresh, menu);
        ImageView locButton = (ImageView) menu.findItem(R.id.menuRefresh).getActionView();
        if (locButton != null) {
            locButton.setImageResource(R.drawable.ic_autorenew_white_48dp);
            locButton.setScaleX(IMAGE_SCALE);
            locButton.setScaleY(IMAGE_SCALE);
            locButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.startAnimation(rotation);
                    // set hide animation
                    oldBitmap = Bitmap.createBitmap(listView.getWidth(), listView.getHeight(), Bitmap.Config.ARGB_8888);
                    canvas = new Canvas(oldBitmap);
                    listView.draw(canvas);
                    upperImage.setImageBitmap(oldBitmap);
                    upperImage.setVisibility(View.VISIBLE);
                    upperImage.startAnimation(hideOldAnimation);
                    upperImage.setVisibility(View.INVISIBLE);
                    // create and use new data set
                    adapter.updateData(QuotesProvider.getQuotesRandom(getActivity()));
                    adapter.notifyDataSetChanged();
                    layoutManager.scrollToPosition(0);
                    // set show new data animation
                    listView.startAnimation(appearNewAnimation);
                    listView.setVisibility(View.VISIBLE);
                }
            });
        }
        super.onCreateOptionsMenu(menu, inflater);
    }
}