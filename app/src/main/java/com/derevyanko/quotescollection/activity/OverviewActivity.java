package com.derevyanko.quotescollection.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import butterknife.InjectView;
import com.crashlytics.android.Crashlytics;
import com.derevyanko.quotescollection.R;
import com.derevyanko.quotescollection.adapter.QuotesProvider;
import com.derevyanko.quotescollection.eventbus.EventCollection;
import com.derevyanko.quotescollection.fragment.FavoriteQuotesFragment;
import com.derevyanko.quotescollection.fragment.NavigationDrawerFragment;
import com.derevyanko.quotescollection.fragment.RandomQuotesFragment;
import com.derevyanko.quotescollection.fragment.SettingsFragment;
import com.derevyanko.quotescollection.util.QuotePreferences;
import com.derevyanko.quotescollection.util.notification.NotificationFactory;
import de.greenrobot.event.EventBus;

public class OverviewActivity extends BaseButterKnifeActivity {

    private static final String ARG_CURRENT_DRAWER = "ARG_CURRENT_DRAWER";

    private ActionBarDrawerToggle mDrawerToggle;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    private EventCollection.DrawerEvent currentEventPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Crashlytics.start(this);
        configureActionBar();
        setQuoteOfDayIfNeeded();

        NavigationDrawerFragment fragment = (NavigationDrawerFragment) getSupportFragmentManager()
                .findFragmentById(R.id.container);
        if (fragment == null) {
            NavigationDrawerFragment mNavigationDrawerFragment = new NavigationDrawerFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, mNavigationDrawerFragment).commit();
        }
    }

    private void setQuoteOfDayIfNeeded() {
        QuotePreferences preferences = new QuotePreferences(this);
        if (preferences.isNeedToShowNotification()) {
            NotificationFactory.setQuoteOfTheDay(this);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_overview;
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    private void configureActionBar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        mDrawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, mToolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close
        );
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        drawerLayout.setDrawerListener(mDrawerToggle);
    }

    @SuppressWarnings("unused")
    public void onEvent(EventCollection.TitleEvent titleEvent) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(titleEvent.getTitle());
        }
    }

    @SuppressWarnings("unused")
    public void onEvent(EventCollection.DrawerEvent drawerEvent) {
        if (currentEventPosition == null || currentEventPosition != drawerEvent) {
            Fragment fragment;
            switch (drawerEvent) {
                case FIRST_SECTION:
                    fragment = RandomQuotesFragment.newInstance(QuotesProvider.getQuotesRandom(this));
                    break;
                case SECOND_SECTION:
                    fragment = FavoriteQuotesFragment.newInstance(QuotesProvider.getQuotesFavorites(this));
                    break;
                default:
                case THIRD_SECTION:
                    fragment = SettingsFragment.newInstance();
                    break;
            }
            drawerLayout.closeDrawers();
            currentEventPosition = drawerEvent;

            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.drawer_container, fragment)
                    .commit();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(ARG_CURRENT_DRAWER, currentEventPosition);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        currentEventPosition = (EventCollection.DrawerEvent) savedInstanceState.getSerializable(ARG_CURRENT_DRAWER);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }
}
