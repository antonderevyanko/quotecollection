package com.derevyanko.quotescollection.activity;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.InjectView;
import butterknife.OnClick;
import com.derevyanko.quotescollection.R;
import com.derevyanko.quotescollection.adapter.QuotesProvider;
import com.derevyanko.quotescollection.database.QuotesDb;
import com.derevyanko.quotescollection.entity.Quote;
import com.derevyanko.quotescollection.eventbus.EventCollection;
import de.greenrobot.event.EventBus;

public class QuoteDetailActivity extends BaseButterKnifeActivity {

    public static final String KEY_CITATION = "citation";
    public static final String KEY_POSITION = "position";

    @InjectView(R.id.citation)
    TextView cite;
    @InjectView(R.id.info)
    TextView info;
    @InjectView(R.id.theme)
    TextView theme;
    @InjectView(R.id.author)
    TextView author;
    @InjectView(R.id.favorite)
    ImageView favorite;
    @InjectView(R.id.share)
    ImageView share;
    @InjectView(R.id.cardView)
    CardView cardView;

    private boolean isFavorite;
    private int positionInList;
    private Quote quote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        quote = (Quote) getIntent().getExtras().getSerializable(KEY_CITATION);
        positionInList = getIntent().getIntExtra(KEY_POSITION, 0);
        setData();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_quote_detail;
    }

    // todo consider to avoid code duplication
    public void setData() {
        isFavorite = QuotesDb.get(this).isFavorite(quote.id);
        initFavoriteState(isFavorite);
        cite.setText(quote.quote);
        info.setText(quote.info);
        theme.setText(quote.theme);
        author.setText(quote.author);
        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFavorite = !isFavorite;
                changeFavoriteState(isFavorite);
                QuotesProvider.setFavoriteState(QuoteDetailActivity.this, quote.id, isFavorite);
                EventBus.getDefault().postSticky(new EventCollection.NotifyAdapterEvent(isFavorite, positionInList));
            }
        });
    }

    private void initFavoriteState(boolean isFavorite) {
        if (isFavorite) {
            favorite.setBackgroundResource(R.drawable.star_anim_frame8);
        } else {
            favorite.setBackgroundResource(R.drawable.star_anim_frame0);
        }
    }

    private void changeFavoriteState(boolean isFavorite) {
        AnimationDrawable animationDrawable;
        if (isFavorite) {
            favorite.setBackgroundResource(R.drawable.star_on_animation);
        } else {
            favorite.setBackgroundResource(R.drawable.star_off_animation);
        }
        animationDrawable = (AnimationDrawable) favorite.getBackground();
        animationDrawable.start();
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.share)
    void onShareClick() {
        share.setBackgroundResource(R.drawable.share_on_animation);
        AnimationDrawable animationDrawable = (AnimationDrawable) share.getBackground();
        animationDrawable.start();
        shareQuote();
    }

    private void shareQuote() {
        String shareBody = quote.quote;
        String shareTitle = quote.author + ":";
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareTitle);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_chooser_title)));
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.container)
    void onContainerClick() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
        }
    }
}
