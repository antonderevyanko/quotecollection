package com.derevyanko.quotescollection.entity;

import android.database.Cursor;

import com.derevyanko.quotescollection.database.DataBaseHelper;

import java.io.Serializable;

public class Quote implements Serializable {

    public String id;
    public String author;
    public int category;
    public String theme;
    public String quote;
    public String info;

    public Quote() {}

    public Quote(Cursor cursor) {
        id = DataBaseHelper.FieldString(cursor, DataBaseHelper.Quotes._id.toString());
        author = DataBaseHelper.FieldString(cursor, DataBaseHelper.Quotes.author.toString());
        theme = DataBaseHelper.FieldString(cursor, DataBaseHelper.Quotes.theme.toString());
        quote = DataBaseHelper.FieldString(cursor, DataBaseHelper.Quotes.citation.toString());
        info = DataBaseHelper.FieldString(cursor, DataBaseHelper.Quotes.info.toString());
        category = DataBaseHelper.FieldInt(cursor, DataBaseHelper.Quotes.category.toString());
    }

}
