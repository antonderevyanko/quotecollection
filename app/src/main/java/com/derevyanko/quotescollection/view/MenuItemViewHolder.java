package com.derevyanko.quotescollection.view;

import android.view.View;
import android.widget.TextView;
import butterknife.InjectView;
import com.derevyanko.quotescollection.R;
import com.derevyanko.quotescollection.util.AbstractViewHolder;

public final class MenuItemViewHolder extends AbstractViewHolder<String> {

    @InjectView(R.id.tvMenuItem)
    TextView item;

    public MenuItemViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bindData(String dataSource) {
        item.setText(dataSource);
    }
}
