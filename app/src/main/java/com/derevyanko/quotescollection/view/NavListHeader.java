package com.derevyanko.quotescollection.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import butterknife.ButterKnife;
import com.derevyanko.quotescollection.R;

public class NavListHeader extends RelativeLayout {

    public NavListHeader(Context context) {
        super(context);
        initView(context);
    }

    public NavListHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        inflate(context, R.layout.view_nav_header, this);
        ButterKnife.inject(this);
        setBackgroundColor(getResources().getColor(R.color.accent));
    }
}
