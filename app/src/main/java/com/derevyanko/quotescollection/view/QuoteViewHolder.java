package com.derevyanko.quotescollection.view;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.derevyanko.quotescollection.R;
import com.derevyanko.quotescollection.activity.QuoteDetailActivity;
import com.derevyanko.quotescollection.adapter.QuotesProvider;
import com.derevyanko.quotescollection.database.QuotesDb;
import com.derevyanko.quotescollection.entity.Quote;
import com.derevyanko.quotescollection.eventbus.EventCollection;
import de.greenrobot.event.EventBus;

public class QuoteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @InjectView(R.id.citation)
    TextView cite;
    @InjectView(R.id.info)
    TextView info;
    @InjectView(R.id.theme)
    TextView theme;
    @InjectView(R.id.author)
    TextView author;
    @InjectView(R.id.favorite)
    ImageView favorite;
    @InjectView(R.id.share)
    ImageView share;
    @InjectView(R.id.cardView)
    CardView cardView;

    private final Context context;
    private boolean isFavorite;
    private Quote quote;

    public QuoteViewHolder(View itemView) {
        super(itemView);
        ButterKnife.inject(this, itemView);
        this.context = itemView.getContext();
        itemView.setOnClickListener(this);
    }

    public void setData(final Quote quote) {
        this.quote = quote;
        isFavorite = QuotesDb.get(context).isFavorite(quote.id);
        initFavoriteState(isFavorite);
        cite.setText(quote.quote);
        info.setText(quote.info);
        theme.setText(quote.theme);
        author.setText(quote.author);
        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFavorite = !isFavorite;
                changeFavoriteState(isFavorite);
                QuotesProvider.setFavoriteState(context, quote.id, isFavorite);
                EventCollection.FavoriteClick event = new EventCollection.FavoriteClick(isFavorite, getPosition());
                EventBus.getDefault().post(event);
            }
        });
    }

    private void initFavoriteState(boolean isFavorite) {
        if (isFavorite) {
            favorite.setBackgroundResource(R.drawable.star_anim_frame8);
        } else {
            favorite.setBackgroundResource(R.drawable.star_anim_frame0);
        }
    }

    private void changeFavoriteState(boolean isFavorite) {
        if (isFavorite) {
            favorite.setBackgroundResource(R.drawable.star_on_animation);
        } else {
            favorite.setBackgroundResource(R.drawable.star_off_animation);
        }
        AnimationDrawable animationDrawable = (AnimationDrawable) favorite.getBackground();
        animationDrawable.start();
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.share)
    void onShareClick() {
        share.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
        share.setBackgroundResource(R.drawable.share_on_animation);
        AnimationDrawable animationDrawable = (AnimationDrawable) share.getBackground();
        animationDrawable.start();
        shareQuote();
    }

    private void shareQuote() {
        String shareBody = quote.quote;
        String shareTitle = quote.author + ":";
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareTitle);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent, context.getResources().getString(R.string
                .share_chooser_title)));
    }

    @Override
    public void onClick(View v) {
        if (quote != null) {
            Context context = v.getContext();

            Intent intent = new Intent(v.getContext(), QuoteDetailActivity.class);
            intent.putExtra(QuoteDetailActivity.KEY_CITATION, quote);
            // position is needed is we currently on the favorite fragment to remove quote from list
            intent.putExtra(QuoteDetailActivity.KEY_POSITION, getPosition());

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) context,
                        Pair.create((View) cardView, cardView.getTransitionName()));
                context.startActivity(intent, options.toBundle());
            } else {
                context.startActivity(intent);
            }
        }
    }
}
