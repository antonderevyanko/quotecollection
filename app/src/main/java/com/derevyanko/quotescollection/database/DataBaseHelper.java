package com.derevyanko.quotescollection.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static String DATABASE_NAME = "citations.sqlite";

    private final static int DATABASE_VERSION = 1;

    public SQLiteDatabase dataBase;
    private final Context dbContext;

    public static final String TABLE_QUOTES = "citations";
    public static final String TABLE_AUTHORS = "authors";
    public static final String TABLE_FAVORITE = "favorite";

    private static DataBaseHelper instance;

    public static synchronized DataBaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DataBaseHelper(context);
        }
        return instance;
    }

    private DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.dbContext = context;

        if (checkDataBaseExist()) {
            openDataBase();
            checkDatabaseVersion();
        } else {
            try {
                this.getReadableDatabase();
                copyDataBase();
                this.close();
                openDataBase();
                dataBase.setVersion(DATABASE_VERSION);
            } catch (IOException e) {
                e.printStackTrace();
                throw new Error("Error copying database");
            }
        }
    }

    public static enum Quotes {
        _id,
        category,
        author,
        theme,
        citation,
        info
    }

    public static enum Authors {
        rowid,
        short_name,
        full_name,
        living_date,
        info,
        full_wiki
    }

    public static enum Favorite {
        rowid,
        quote_id,
        favorite
    }

    public static String FieldString(Cursor cur, String name) {
        try {
            return cur.getString(cur.getColumnIndex(name));
        } catch (IllegalStateException e) {
            return "";
        }
    }

    public static int FieldInt(Cursor cur, String name) {
        try {
            return cur.getInt(cur.getColumnIndex(name));
        } catch (IllegalStateException e) {
            return 0;
        }
    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     * */
    private void copyDataBase() throws IOException {
        InputStream myInput = dbContext.getAssets().open(DATABASE_NAME);
        String outFileName = getPath();
        OutputStream myOutput = new FileOutputStream(outFileName);

        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }

        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    private String getPath() {
        return dbContext.getDatabasePath(DATABASE_NAME).getPath();
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        if (dataBase != null)
            if (dataBase.isOpen()) {
                dataBase.close();
            }
        dataBase = SQLiteDatabase.openDatabase(getPath(), null,
                SQLiteDatabase.OPEN_READWRITE);
        return dataBase;
    }

    private boolean checkDataBaseExist() {
        SQLiteDatabase checkDB = null;
        try {
            checkDB = SQLiteDatabase.openDatabase(getPath(), null, SQLiteDatabase.OPEN_READWRITE);
        } catch (SQLiteException error) {
            error.printStackTrace();
        }
        if (checkDB != null) checkDB.close();
        return checkDB != null;
    }

    @Override
    public void onCreate(SQLiteDatabase db) { }

    public void checkDatabaseVersion() {
        //  check database version if needed
        openDataBase();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { }

}
