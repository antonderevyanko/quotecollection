package com.derevyanko.quotescollection.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class QuotesDb {

    private static QuotesDb instance;
    private final SQLiteDatabase db;

    private QuotesDb(SQLiteDatabase sqLiteDatabase) {
        this.db = sqLiteDatabase;
    }

    public static synchronized QuotesDb get(Context context) {
        if (instance == null) {
            DataBaseHelper helper = DataBaseHelper.getInstance(context);
            instance = new QuotesDb(helper.openDataBase());
        }
        return instance;
    }

    public void closeDb() {
        this.db.close();
        instance = null;
    }

    /**
     * @return Cursor which contains random N quotes
     */
    public Cursor getQuotesRandom(int quantity) {
        return db.query(DataBaseHelper.TABLE_QUOTES, null, null, null,
                null, null, "RANDOM() LIMIT " + String.valueOf(quantity)  , null);
    }

    /**
     * Search quotes with id which places in the range from <= id => to
     * @return Cursor which contains quotes
     */
    public Cursor getQuotesInRange(int from, int to) {
        return db.query(DataBaseHelper.TABLE_QUOTES, null,
                "_id >= " + String.valueOf(from) + " and _id <= " + String.valueOf(to),
                null, null, null, null);
    }

    /**
     * Search and returns quotes made by author
     * @param author - String, author name
     * @return Cursor with quotes which belongs to author
     */
    public Cursor getQuotesByAuthor(String author) {
        return db.query(DataBaseHelper.TABLE_QUOTES, null,
                "author = '" + String.valueOf(author) + "'",
                null, null, null, null);
    }

    public Cursor getQuotesWidget() {
        return db.query(DataBaseHelper.TABLE_QUOTES, new String[] {
                DataBaseHelper.Quotes.author.toString(),
                DataBaseHelper.Quotes._id.toString(),
                DataBaseHelper.Quotes.citation.toString(),
                DataBaseHelper.Quotes.info.toString()}, null, null, null, null, "RANDOM() LIMIT 50", null);
    }

    /**
     * @return quotes for all authors
     */
    public Cursor getAuthorsAll() {
        return db.query(DataBaseHelper.TABLE_QUOTES, new String[] {
                DataBaseHelper.Quotes.author.toString()}, null, null, null, null, null);
    }

    /**
     * @param category
     * @return Cursor with quotes by all authors in category
     */
    public Cursor getAuthorsByCategory(int category) {
        return db.query(DataBaseHelper.TABLE_QUOTES, null,
                DataBaseHelper.Quotes.category.toString() + "='" + Integer.toString(category) +"'",
                null, null, null, null);
    }

    /**
     * Search quotes which contains word
     * @param query - word to search
     * @return Cursor with quotes
     */
    public Cursor searchQuotes(String query) {
        return db.query(DataBaseHelper.TABLE_QUOTES,
                new String[] { DataBaseHelper.Quotes._id.toString(),
                        DataBaseHelper.Quotes.citation.toString(),
                        DataBaseHelper.Quotes.author.toString()},
                DataBaseHelper.Quotes.citation.toString() + " LIKE" + "'%" + query + "%'",
                null, null, null,
                DataBaseHelper.Quotes.author.toString(), null);
    }

    public Cursor searchAuthors(String query) {
        return db.query(DataBaseHelper.TABLE_QUOTES,
                new String[] { DataBaseHelper.Quotes._id.toString(),
                        DataBaseHelper.Quotes.author.toString()},
                DataBaseHelper.Quotes.author.toString() + " LIKE" + "'%" + query + "%'",
                null, DataBaseHelper.Quotes.author.toString(), null,
                null, null);
    }

    public Cursor searchQuotesWithAuthor(String query, String author) {
        return db.query(DataBaseHelper.TABLE_QUOTES,
                new String[] { DataBaseHelper.Quotes._id.toString(),
                        DataBaseHelper.Quotes.citation.toString(),
                        DataBaseHelper.Quotes.author.toString()},
                DataBaseHelper.Quotes.citation.toString() + " LIKE" + "'%" + query + "%' AND " +
                        DataBaseHelper.Quotes.author.toString() + " ='" + author +"'",
                null, null, null,
                DataBaseHelper.Quotes.author.toString(), null);
    }

    public void setFavoriteState(String quoteId, boolean isNewStateFavorite) {
        if (isNewStateFavorite) {
            setFavorite(quoteId);
        } else {
            deleteFavorite(quoteId);
        }
    }

    public Cursor getFavoriteQuotes() {
        return db.query(DataBaseHelper.TABLE_QUOTES + ',' + DataBaseHelper.TABLE_FAVORITE, null,
                DataBaseHelper.Quotes._id.toString() + '=' + DataBaseHelper.Favorite.quote_id.toString(),
                null, null, null, DataBaseHelper.Quotes._id.toString());
    }

    public boolean isFavorite(String quoteId) {
        Cursor cursor = db.query(DataBaseHelper.TABLE_FAVORITE, null,
                DataBaseHelper.Favorite.quote_id.toString() + "=?",
                new String[] {quoteId},
                null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int count = cursor.getCount();
            cursor.close();
            if ( count != 0 )
                return true;
        }
        return false;
    }

    private void setFavorite(String quoteId) {
        if (isFavorite(quoteId))
            return;
        ContentValues updateValues = new ContentValues();
        updateValues.put(DataBaseHelper.Favorite.quote_id.toString(), quoteId);
        updateValues.put(DataBaseHelper.Favorite.favorite.toString(), true);
        db.insert(DataBaseHelper.TABLE_FAVORITE, null, updateValues);
    }

    private void deleteFavorite(String quoteId) {
        if (!isFavorite(quoteId))
            return;
        db.delete(DataBaseHelper.TABLE_FAVORITE,
                DataBaseHelper.Favorite.quote_id.toString() + "=?",
                new String[] {quoteId});
    }
}
