package com.derevyanko.quotescollection.util;

import android.util.Log;
import com.derevyanko.quotescollection.BuildConfig;

public final class MyLog {

    private static final String TAG = "QC";
    private static final boolean IS_DEBUG = BuildConfig.DEBUG;

    private MyLog() {
    }

    @SuppressWarnings("unused")
    public static void logStackTrace(Throwable ex) {
        warning(TAG, ex.toString());
        for (StackTraceElement element : ex.getStackTrace()) {
            warning(TAG, element.toString());
        }
    }

    @SuppressWarnings("unused")
    public static void verbose(String tag, String message) {
        runLogs(Log.VERBOSE, tag, message);
    }

    @SuppressWarnings("unused")
    public static void debug(String tag, String message) {
        runLogs(Log.DEBUG, tag, message);
    }

    @SuppressWarnings("unused")
    public static void info(String tag, String message) {
        runLogs(Log.INFO, tag, message);
    }

    public static void warning(String tag, String message) {
        runLogs(Log.WARN, tag, message);
    }

    @SuppressWarnings("unused")
    public static void error(String tag, String message) {
        runLogs(Log.ERROR, tag, message);
    }

    private static void runLogs(int priority, String tag, String message) {
        if (IS_DEBUG) {
            Log.println(priority, tag, message);
        }
    }
}
