package com.derevyanko.quotescollection.util;

import android.content.res.Resources;
import android.view.View;
import butterknife.ButterKnife;

public abstract class AbstractViewHolder<T> {

    protected final Resources resources;

    public AbstractViewHolder(View itemView) {
        ButterKnife.inject(this, itemView);
        this.resources = itemView.getResources();
    }

    public abstract void bindData(T dataSource);

}