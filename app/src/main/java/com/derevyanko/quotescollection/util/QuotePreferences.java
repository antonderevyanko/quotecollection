package com.derevyanko.quotescollection.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public final class QuotePreferences {

    private static final String IS_SHOW_NOTIFICATION = "is_show_notification";
    private final SharedPreferences sharedPreferences;

    public QuotePreferences(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setShowNotification(boolean isShow) {
        sharedPreferences.edit().putBoolean(IS_SHOW_NOTIFICATION, isShow).apply();
    }

    public boolean isNeedToShowNotification() {
        return sharedPreferences.getBoolean(IS_SHOW_NOTIFICATION, false);
    }
}
