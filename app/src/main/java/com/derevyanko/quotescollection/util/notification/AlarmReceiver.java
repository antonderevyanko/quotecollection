package com.derevyanko.quotescollection.util.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.derevyanko.quotescollection.adapter.QuotesProvider;
import com.derevyanko.quotescollection.entity.Quote;
import com.derevyanko.quotescollection.util.QuotePreferences;

public final class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        QuotePreferences preferences = new QuotePreferences(context);
        if (preferences.isNeedToShowNotification()) {
            NotificationFactory.showNotification(context, getRandomQuote(context));
        } else {
            NotificationFactory.removeAllNotifications(context);
        }
    }

    private Quote getRandomQuote(Context context) {
        return QuotesProvider.getQuoteRandom(context);
    }
}
