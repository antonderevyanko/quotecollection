package com.derevyanko.quotescollection.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

public abstract class AbstractArrayAdapter<T, V extends AbstractViewHolder<T>> extends ArrayAdapter<T> {

    protected final LayoutInflater inflater;
    protected final int itemLayoutId;

    public AbstractArrayAdapter(Context context, int itemLayoutId, List<T> objects) {
        super(context, 0, objects);
        this.itemLayoutId = itemLayoutId;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public final View getView(int position, View convertView, ViewGroup parent) {
        View result;
        V viewHolder;
        if (convertView == null) {
            result = inflater.inflate(itemLayoutId, parent, false);
            viewHolder = constructHolderView(result);
            result.setTag(viewHolder);
        } else {
            result = convertView;
            try {
                viewHolder = (V) convertView.getTag();
            } catch (ClassCastException e) {
                MyLog.error("array adapter", "cannot get view from the tag");
                viewHolder = constructHolderView(result);
                result.setTag(viewHolder);
            }
        }
        viewHolder.bindData(getItem(position));
        return result;
    }

    protected abstract V constructHolderView(View view);
}
