package com.derevyanko.quotescollection.util.notification;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import com.derevyanko.quotescollection.R;
import com.derevyanko.quotescollection.activity.QuoteDetailActivity;
import com.derevyanko.quotescollection.entity.Quote;

import java.util.Calendar;

public final class NotificationFactory {

    private static final int TEXT_LENGTH = 60;
    private static final int INTERVAL_MILLIS = 60 * 1000;

    public static void setQuoteOfTheDay(Context context) {
        Calendar calendar = Calendar.getInstance();
        // todo uncomment before publish
//        calendar.set(Calendar.HOUR_OF_DAY, 13);
//        calendar.set(Calendar.MINUTE, 0);
//        calendar.set(Calendar.SECOND, 0);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                new Intent(context, AlarmReceiver.class), PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                INTERVAL_MILLIS, pendingIntent);
    }

    public static void showNotification(Context context, Quote quote) {
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = makeDefault(context, quote);
        manager.notify(0, builder.build());
    }

    public static void removeAllNotifications(Context context) {
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancelAll();
    }

    private static NotificationCompat.Builder makeDefault(Context context, Quote quote) {
        Intent intent = new Intent(context, QuoteDetailActivity.class);
        intent.putExtra(QuoteDetailActivity.KEY_CITATION, quote);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return new NotificationCompat.Builder(context)
                .setContentTitle(quote.author)
                .setContentText(quote.quote)
                .setSmallIcon(R.drawable.ic_launcher)
                .setColor(context.getResources().getColor(R.color.accent))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(quote.quote))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setTicker(getTickerText(quote));
    }

    private static String getTickerText(Quote quote) {
        String tickerText = quote.author + ": " + quote.quote;
        if (tickerText.length() >= TEXT_LENGTH) {
            return tickerText.substring(0, TEXT_LENGTH) + "...";
        } else {
            return tickerText;
        }
    }
}
