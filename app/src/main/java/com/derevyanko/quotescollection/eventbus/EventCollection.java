package com.derevyanko.quotescollection.eventbus;

import java.io.Serializable;

public class EventCollection {

    public enum DrawerEvent implements Serializable {

        FIRST_SECTION(0),
        SECOND_SECTION(1),
        THIRD_SECTION(2);

        int value;

        DrawerEvent(int value) {
            this.value = value;
        }
    }

    public static class TitleEvent {

        private String title;

        public TitleEvent(String title) {
            this.title = title;
        }

        public String getTitle() {
            return title;
        }
    }

    public static class FavoriteClick {

        private int position;
        private boolean isChecked;

        public FavoriteClick(boolean isChecked, int position) {
            this.position = position;
            this.isChecked = isChecked;
        }

        public int getItemPosition() {
            return position;
        }

        public boolean isChecked() {
            return isChecked;
        }
    }

    public static final class NotifyAdapterEvent {

        private int position;
        private boolean isChecked;

        public NotifyAdapterEvent(boolean isChecked, int position) {
            this.position = position;
            this.isChecked = isChecked;
        }

        public int getPosition() {
            return position;
        }

        public boolean isChecked() {
            return isChecked;
        }
    }
}
