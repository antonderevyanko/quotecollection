package com.derevyanko.quotescollection.adapter;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.derevyanko.quotescollection.database.QuotesDb;
import com.derevyanko.quotescollection.entity.Quote;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class QuotesProvider {

    private static final int RANDOM_QUOTES_QUANTITY = 100;

    public static String getQuotesAsString(List<Quote> quotes) {
        Gson qson = new Gson();
        return qson.toJson(quotes);
    }

    public static List<Quote> getQuotesFromString(String source) {
        Gson gson = new Gson();
        List<Quote> quotes = new ArrayList<>();
        try {
            quotes = gson.fromJson(source, new TypeToken<ArrayList<Quote>>() {
            }.getType());
        } catch (JsonParseException parseException) {
            // will return empty list
            Log.e("error parse quotes JSON", parseException.getLocalizedMessage());
        }
        return quotes;
    }

    public static List<Quote> getQuotesRandom(Context context) {
        Cursor curQuotes = QuotesDb.get(context).getQuotesRandom(RANDOM_QUOTES_QUANTITY);
        return constructQuotesFromCursor(curQuotes);
    }

    /**
     * Returns one random Quote
     * @param context
     * @return
     */
    public static Quote getQuoteRandom(Context context) {
        Cursor curQuotes = QuotesDb.get(context).getQuotesRandom(1);
        return constructQuotesFromCursor(curQuotes).get(0);
    }

    public static List<Quote> getQuotesFavorites(Context context) {
        Cursor curQuotes = QuotesDb.get(context).getFavoriteQuotes();
        return constructQuotesFromCursor(curQuotes);
    }

    public static List<Quote> getQuotesByAuthor(Context context, String author) {
        Cursor curQuotes = QuotesDb.get(context).getQuotesByAuthor(author);
        return constructQuotesFromCursor(curQuotes);
    }

    public static void setFavoriteState(Context context, String quoteId, boolean newState) {
        QuotesDb.get(context).setFavoriteState(quoteId, newState);
    }

    private static List<Quote> constructQuotesFromCursor(Cursor cursor) {
        List<Quote> quotes = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Quote quote = new Quote(cursor);
            quotes.add(quote);
            cursor.moveToNext();
        }
        cursor.close();
        return quotes;
    }

//    private void loadQuotesFromCursor(String authors) {
//        Cursor allAuthors = loadData(authors);
//        try {
//            listQuotes = new QuotesList(allAuthors);
//            allAuthors.close();
//            if (getIntent().getStringExtra(FILTER_JUMPTO) != null) {
//                listQuotes.jumpToQuoteWithId(getIntent().getStringExtra(FILTER_JUMPTO));
//            }
//        } catch (Exception e) {
//            showCloseWithWarning(R.string.db_error);
//        }
//    }

}
