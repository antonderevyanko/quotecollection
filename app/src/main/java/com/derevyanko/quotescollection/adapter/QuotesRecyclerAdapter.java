package com.derevyanko.quotescollection.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.derevyanko.quotescollection.R;
import com.derevyanko.quotescollection.entity.Quote;
import com.derevyanko.quotescollection.view.QuoteViewHolder;

import java.util.List;

public class QuotesRecyclerAdapter extends RecyclerView.Adapter<QuoteViewHolder> {

    private List<Quote> quotes;

    public QuotesRecyclerAdapter(List<Quote> quotes) {
        updateData(quotes);
    }

    public void updateData(List<Quote> quotes) {
        this.quotes = quotes;
    }

    @Override
    public QuoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quote_widget, parent, false);
        return new QuoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final QuoteViewHolder holder, int position) {
        holder.setData(quotes.get(position));
    }

    @Override
    public int getItemCount() {
        return quotes.size();
    }

    public void removeItem(int itemPosition) {
        notifyItemRemoved(itemPosition);
        quotes.remove(itemPosition);
    }
}
