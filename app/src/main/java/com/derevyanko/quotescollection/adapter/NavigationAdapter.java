package com.derevyanko.quotescollection.adapter;

import android.content.Context;
import android.view.View;
import com.derevyanko.quotescollection.util.AbstractArrayAdapter;
import com.derevyanko.quotescollection.view.MenuItemViewHolder;

import java.util.List;

public final class NavigationAdapter extends AbstractArrayAdapter<String, MenuItemViewHolder> {

    public NavigationAdapter(Context context, int itemLayoutId, List<String> objects) {
        super(context, itemLayoutId, objects);
    }

    @Override
    protected MenuItemViewHolder constructHolderView(View view) {
        return new MenuItemViewHolder(view);
    }

}
